
import java.util.*;
public class modul3Crypto {
    public static String s;
    public static int shift=0;
    public static int groupSize=0;

    public static void main(String[] args) {
        s=encryptString();
        s=normalizedText();
        s=caesarify();
        s=groupify();
        System.out.println(s);
    }

    //this method receive from user: string, group zise and shifting key
    public static String encryptString (){
        Scanner input = new Scanner(System.in);
        System.out.println("******Hallo*******\n");
        System.out.println("please enter a string\n");
        s = input.nextLine();
        System.out.println(s);
        System.out.println("please enter a shift value\n");
        shift=input.nextInt();
        System.out.println(shift);
        System.out.println("please enter a group size value\n");
        groupSize=input.nextInt();
        System.out.println(groupSize);
        return (s);
    }

    //this methos erases wrong charecters
    public static String normalizedText(){
               s = s.replace(" ","");
               s = s.replace(".","");
               s = s.replace(",","");
               s = s.replace(";","");
               s = s.replace(":","");
               s = s.replace("(","");
               s = s.replace(")","");
               s = s.replace("?","");
               s = s.replace("!","");
               s = s.replace("\"","");
               s=s.toUpperCase();
               return (s);
    }

    //this method mekes the encryption by shifting each charecter
    public static String caesarify(){
        for(int i=0; i<s.length(); i++){
            int index = s.charAt(i);
            int newIndex=index+shift;
            if(newIndex<65){
                newIndex=91-(65-newIndex);
            }else if(newIndex>90){
                newIndex=64+(newIndex-90);
            }
            char newChar= (char) newIndex;
            s=s.substring(0, i) + newChar + s.substring(i+1);
        }
        return(s);
    }

   //this method devides to groups with xxxx at the end if needed
    public static String groupify(){
        int length=s.length();
        int add=groupSize-length%groupSize;
        /****************** adding xxxx as needded  */
        for (int i=0; i<add; i++) {
            s = s + 'x';
        }
        /****************** adding spaces as needded  */
            int nos=(s.length()-1)/groupSize;
            for (int i=groupSize; i<s.length(); i=i+groupSize+1 ){
                s=s.substring(0, i) + ' ' + s.substring(i);
                System.out.println(s);
            }
        return(s);
    }

}
